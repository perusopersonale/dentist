import supertest from 'supertest';
import StatusCodes from 'http-status-codes';
import { SuperTest, Test } from 'supertest';

import app from '@server';
import { login, registerUser } from '../support/LoginAgent';
import mongoose from 'mongoose';
import User, { UserRoles } from '@models/User';
import { paramMissingError } from '@utils/constants';
import { IReqBody } from '../support/types';


const userData = {
	email: 'test@gmail.com',
	firstName: 'test',
	lastName: 'test',
	password: 'Password@1',
};

describe('UserRouter', () => {

	const { BAD_REQUEST, OK, UNAUTHORIZED } = StatusCodes;

	const usersPath = '/api/users';

	let agent: SuperTest<Test>;
	let jwtCookie: string;



	beforeEach(async function () {
		const instance = await mongoose.connect(
				String(process.env.DB_CONNECTION_STRING),
				{ useNewUrlParser: true }
			)
		
		const collections = await instance.connection.db.collections()
		for (let collection of collections) {
			await collection.drop()
		}
		agent = await supertest.agent(app);
	})
  describe(`"GET:${usersPath}"`, () => {
		beforeEach(async function () {
			await registerUser(agent)
			jwtCookie = await login(agent)
		})
		const callApi = async () => {
			return agent.get(usersPath).set('Cookie', jwtCookie);
		};


		it(`should return "${UNAUTHORIZED}" if the request is made by a non-admin user`, async () => {
			const res = await callApi()
			expect(res.status).toBe(UNAUTHORIZED);
		});
		it(`should return a JSON object with all the users and a status code of "${OK}" if the
		request was successful.`, async () => {
			//creating admin
			const { id } = await registerUser(agent, userData)
		 	await User.updateOne({ _id: id}, { role: UserRoles.Admin})
			jwtCookie = await login(agent, userData)

			const res = await callApi()
			expect(res.status).toBe(OK);
			// 2 the default one created and the admin
			expect(res.body.data.length).toBe(2)
		});
	});


	describe(`"POST:${usersPath}"`, () => {
		
		const callApi = (reqBody: IReqBody) => {
			return agent.post(usersPath).type('form').send(reqBody);
		};

		const userData = {
			email: 'jsmith@gmail.com',
			firstName: 'test',
			lastName: 'test',
			password: 'Password@1',
		};

		it(`should return 200 if the request was successful.`, async () => {  
			const res = await callApi(userData)
		  expect(res.status).toBe(200);
		  expect(res.body.error).toBeUndefined();  
		});


	   it(`should return a JSON object with an error message of "${paramMissingError}" and a status
			code of "${BAD_REQUEST}" if the user param was missing.`, async () => {
				const res = await callApi({})
				expect(res.status).toBe(BAD_REQUEST);
				expect(res.body.error).toBe(paramMissingError);
		});


		it(`should prevent the same email to be used more than once.`, async () => {
			const res = await callApi(userData)
		  expect(res.status).toBe(200);
		  expect(res.body.error).toBeUndefined(); 
			const res1 = await callApi(userData)
		  expect(res1.status).toBe(BAD_REQUEST);
		});
	});


	describe(`"PUT:${usersPath}"`, () => {
		interface updateProfile {
			firstName?: string,
			lastName?: string
		}
		let user: any
		beforeEach(async function () {
			user = await registerUser(agent)
			jwtCookie = await login(agent)
		})
		const callApi = (reqBody: updateProfile, id: string ) => {
			return agent.put(`${usersPath}/${id}`).set('Cookie', jwtCookie).type('form').send(reqBody);
		};

		it(`should return a status code of "${UNAUTHORIZED}" if try to update profile and authenticated.`, async () => {
			const res = await callApi({firstName: "test"}, user.id)
			expect(res.status).toBe(UNAUTHORIZED);
		});
		it(`should return a status code of "${UNAUTHORIZED}" if try to update profile and not authenticated.`, async () => {
			//creating admin
			const { id } = await registerUser(agent, userData)
			await User.updateOne({ _id: id}, { role: UserRoles.Admin})
			jwtCookie = await login(agent, userData)
 
			const res = await callApi({firstName: "gigi"}, user.id)
			expect(res.status).toBe(OK);
			const upUser = await User.findById(user.id)
			expect(upUser?.firstName).toBe("gigi")
		});

	})

 
	/*describe(`"DELETE:${usersPath}"`, () => {

	
		let user: any
		beforeEach(async function () {
			user = await registerUser(agent)
			jwtCookie = await login(agent)
		})
		const callApi = ( id: string) => {
			return agent.delete(`${usersPath}/${id}`).set('Cookie', jwtCookie).type('form').send();
		};

		it(`should return a status code of "${UNAUTHORIZED}" if try to update profile and not admin.`, async () => {
			const res = await callApi(user.id)
			expect(res.status).toBe(UNAUTHORIZED);
		});
		it(`should return a status code of "${OK}" if try to update profile and admin.`, async () => {
			const userIdTodel = user.id
			//creating admin
			const { id } = await registerUser(agent, userData)
			await User.updateOne({ _id: id}, { role: UserRoles.Admin})
			jwtCookie = await login(agent, userData)
			const res = await callApi( userIdTodel)
			expect(res.status).toBe(OK);
			const users = await User.find()
			expect(users.length).toBe(1)
		});
	})*/
});
