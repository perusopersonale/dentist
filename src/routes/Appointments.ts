import StatusCodes from 'http-status-codes';
import { Request, Response, Router } from 'express';

import validate from '@middlewares/validator';
import { UserRoles } from '@models/User';
import { restrictAccessTo } from '@middlewares/auth';
import Appointment from '@models/Appointment';
import { addDayToDate } from '@utils/functions';
import logger from '@utils/Logger';

const router = Router();
const { BAD_REQUEST,  OK , NOT_FOUND, UNAUTHORIZED} = StatusCodes;

/******************************************************************************
 *                      Get All Users - "GET /api/appointments/"
 ******************************************************************************/

router.get('/*', restrictAccessTo(UserRoles.Standard), async (req: Request, res: Response) => {
	const now = new Date()

	const start = req.query.start
	const end = req.query.end

	const intervalStart = new Date(start as string) || now
	const intervalEnd = end? new Date(end as string) : addDayToDate(intervalStart, 7)
	
	const filter : any = {
		startDate: { $gte: intervalStart, $lte: intervalEnd },
		endDate: { $gte: intervalStart, $lte: intervalEnd},
	}
	const appointments = await Appointment.find(filter)
	const appsWithoutSensibleData = appointments.map( ({ id, userId, startDate, endDate, description, title}) => {
		const base = { id, startDate, endDate, }
		return userId === req.user.id ? {...base, userId, description, title } : base
	})
	return res.json({ data: appsWithoutSensibleData });
});



/******************************************************************************
 *                       Add One - "POST /api/appointments/"
 ******************************************************************************/

router.post('/', validate('createAppointment'), restrictAccessTo(UserRoles.Standard), async (req: Request, res: Response) => {
	try{
		const appDoc = new Appointment(req.body)
		appDoc.userId = req.user.id
		await appDoc.save()
		
		logger.info(`Created new appointment`)
		return res.json({ data: {id: appDoc.id } });
	} catch(err){
		res.status(BAD_REQUEST).json({ error: err.message })
	}
});



/******************************************************************************
 *                       Update - "PUT /api/appointments/:id"
 ******************************************************************************/

router.put('/:_id', restrictAccessTo(UserRoles.Standard), validate('updateAppointment'), async (req: Request, res: Response) => {
	const { _id } = req.params;
	const appointment = await Appointment.findById(_id)
	if(!appointment){
		res.status(NOT_FOUND).end()
		return
	}
	if(appointment.userId !== req.user.id) {
		res.status(UNAUTHORIZED).end()
		return
	}

	await Appointment.updateOne({_id}, req.body)
	return res.status(OK).json({ data: { id: _id}});
});


/******************************************************************************
 *                    Delete - "DELETE /api/appointments/:id"
 ******************************************************************************/

router.delete('/:id', restrictAccessTo(UserRoles.Standard), async (req: Request, res: Response) => {
	const { id } = req.params;
	const appointment = await Appointment.findById(id)
	if(!appointment){
		res.status(NOT_FOUND).end()
		return
	}
	if(appointment.userId !== req.user.id) {
		res.status(UNAUTHORIZED).end()
		return
	}
	await Appointment.remove({_id: id});
	return res.status(OK).end();
});



/******************************************************************************
 *                                     Export
 ******************************************************************************/

export default router;
