import cookieParser from 'cookie-parser';
import morgan from 'morgan';
import helmet from 'helmet';
import StatusCodes from 'http-status-codes';
import express, { NextFunction, Request, Response } from 'express'; 
import 'express-async-errors';

import BaseRouter from './routes';
import logger from '@utils/Logger';
import { cookieProps } from '@utils/constants';
import { IClientData } from '@utils/JwtService';

import connectToDb from '@models/connect'
import jsonValidator from '@utils/jsonValidator'

declare global {
  namespace Express {
    interface Request {
      user: IClientData
    }
  }
}

//load jsonschema
jsonValidator.init()
// connect to DB
connectToDb()
const app = express();
const { BAD_REQUEST } = StatusCodes;



/************************************************************************************
 *                              Set basic express settings
 ***********************************************************************************/

app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cookieParser(cookieProps.secret));


// Show routes called in console during development
if (process.env.NODE_ENV === 'development') {
  app.use(morgan('dev'));
}

// Security
if (process.env.NODE_ENV === 'production') {
  app.use(helmet());
}

// Add APIs
app.use('/api', BaseRouter);

app.use(function (req, res, next) {
  res.status(404).json({})
})

// Print API errors
// eslint-disable-next-line @typescript-eslint/no-unused-vars
app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
	logger.err(err, true);
	return res.status(BAD_REQUEST).json({
		error: err.message,
  });
});


/************************************************************************************
 *                              Export Server
 ***********************************************************************************/

export default app;
