import bcrypt from 'bcrypt';
import StatusCodes from 'http-status-codes';
import { Request, Response, Router } from 'express';

import validate from '@middlewares/validator';
import User, { UserRoles } from '@models/User';
import { restrictAccessTo } from '@middlewares/auth';
import { pwdSaltRounds } from '@utils/constants';
import logger from '@utils/Logger';

const router = Router();
const { BAD_REQUEST,  OK , NOT_FOUND} = StatusCodes;


/******************************************************************************
 *                      Get All Users - "GET /api/users/"
 ******************************************************************************/

router.get('/', restrictAccessTo(UserRoles.Admin), async (req: Request, res: Response) => {
	const users = await User.find();
	return res.json({data: users});
});



/******************************************************************************
 *                       Add One - "POST /api/users/"
 ******************************************************************************/

router.post('/', validate('createUser'), async (req: Request, res: Response) => {
	try{
		const { email, firstName, lastName, password } = req.body
		// in test unique email seems not to work...
		const user =  await User.findOne({email})
		if(user) throw new Error('Already registered')
		logger.info(`Creating new user with email: ${email}`)
		const pwdHash = await bcrypt.hash(password, pwdSaltRounds)
		const userDoc = new User({ email, firstName, lastName, pwdHash, role: UserRoles.Standard})
		await userDoc.save()
		res.json({id: userDoc._id, email: userDoc.email});
	} catch(err){
		logger.err(err)
		res.status(BAD_REQUEST).json({ error: err.message })
	}
});



/******************************************************************************
 *                       Update - "PUT /api/users/:id"
 ******************************************************************************/

router.put('/:_id', restrictAccessTo(UserRoles.Admin), validate('updateUser'), async (req: Request, res: Response) => {
	const { _id } = req.params;
	try{
		await User.findById(_id)
	} catch(err){
		return res.status(NOT_FOUND)
	}
	await User.updateOne({_id}, req.body)

	return res.status(OK).json({ data: { id: _id } });
});



/******************************************************************************
 *                    Delete - "DELETE /api/users/:id"
 ******************************************************************************/

router.delete('/:_id', restrictAccessTo(UserRoles.Admin), async (req: Request, res: Response) => {
	const { _id } = req.params;
	await User.deleteOne({_id})
	return res.status(OK)
});



/******************************************************************************
 *                                     Export
 ******************************************************************************/

export default router;
