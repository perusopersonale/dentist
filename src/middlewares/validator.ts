import jsonValidator from "@utils/jsonValidator"
import { Request, Response, NextFunction } from 'express';
import StatusCodes from 'http-status-codes';

export default function validate(schema: string){
  return (req: Request, res: Response, next: NextFunction) => {
    try { 
      jsonValidator.validate(schema, req.body)
      next()
    } catch(err){
      return res.status(StatusCodes.BAD_REQUEST).json({
        error: err.message,
      });
    }
  };
}
