import { IUser } from '@models/User';
import { SuperTest, Test } from 'supertest';

const defaultUser = {
	email: 'jsmith@gmail.com',
	firstName: 'test',
	lastName: 'test',
	password: 'Password@1',
};
export interface ILoginReq {
	email: string,
	password: string,
}

export const registerUser = async (beforeAgent: SuperTest<Test>, user?: any ) => {
	const userToRegister = user || defaultUser
	//create user
	const res = await  beforeAgent
		.post('/api/users')
		.type('form')
		.send(userToRegister)
	return res.body
}
export const login = async (beforeAgent: SuperTest<Test>, user?: any ) => {
	const userLogin = user || defaultUser
	const resLogin = await beforeAgent
		.post('/api/auth/login')
		.type('form')
		.send({ email: userLogin.email, password: userLogin.password})
		
	return resLogin.headers['set-cookie'];
}


