import supertest from 'supertest';
import StatusCodes from 'http-status-codes';
import { SuperTest, Test } from 'supertest';

import app from '@server';
import { login, registerUser } from '../support/LoginAgent';
import mongoose from 'mongoose';
import  { IUser } from '@models/User';
import Appointment, { IAppointment } from '@models/Appointment';
import { addHoursToDate } from '@utils/functions';
import QueryString from 'qs';


describe('AppointmentRouter', () => {

	const { BAD_REQUEST, OK, UNAUTHORIZED } = StatusCodes;

	const appointmentsPath = '/api/appointments';

	let agent: SuperTest<Test>;
	let jwtCookie: string;
  let user: IUser


	beforeEach(async function () {
		const instance = await mongoose.connect(
				String(process.env.DB_CONNECTION_STRING),
				{ useNewUrlParser: true }
			)
		
		const collections = await instance.connection.db.collections()
		for (let collection of collections) {
			await collection.drop()
		}
		agent = await supertest.agent(app);
  	user = await registerUser(agent)
		jwtCookie = await login(agent)
	})
  describe(`"GET:${appointmentsPath}"`, () => {
		const callApi = async (query: any) => {
			return agent.get(`${appointmentsPath}?${QueryString.stringify(query)}`).set('Cookie', jwtCookie);
		};
	  /*it(`should return "${UNAUTHORIZED}" and if not logged`, async () => {
      const res = await  agent.get(appointmentsPath)
			expect(res.status).toBe(UNAUTHORIZED);
		});*/
		it(`should return "${OK}" and return appointments if logged and appointment are in query interval`, async () => {
      const startDate = new Date(2020, 2, 18, 9, 0)
      await Appointment.create({
        userId: user.id,
        startDate ,
        endDate: addHoursToDate(startDate, 1)
      })
      await Appointment.create({
        userId: user.id,
        startDate: addHoursToDate(startDate, 2) ,
        endDate: addHoursToDate(startDate, 3)
      })
      const res = await callApi({start: addHoursToDate(startDate, -1), end: addHoursToDate(startDate, 5)})
			expect(res.status).toBe(OK);
			expect(res.body.data.length).toBe(2)
		});
  })
  describe(`"POST:${appointmentsPath}"`, () => {
    const callApi = async (app: any) => {
      return agent.post(appointmentsPath).set('Cookie', jwtCookie).send(app);
    };
    it(`should return "${OK}" and return new appointment id if logged and appointment body is valid`, async () => {
      const startDate = new Date(2020, 2, 18, 9, 0)
      const appointment  = {
        title: "test",
        startDate ,
        endDate: addHoursToDate(startDate, 1)
      }
      
      const res = await callApi(appointment)
      expect(res.status).toBe(OK);
      expect(res.body.data.id).toBeDefined()
    });
    it(`should return "${BAD_REQUEST}" if appointments overlap`, async () => {
      const startDate = new Date(2020, 2, 18, 9, 0)
      const appointment  = {
        title: "test",
        startDate ,
        endDate: addHoursToDate(startDate, 1)
      }
      
      const res = await callApi(appointment)
      expect(res.status).toBe(OK);
      const res1 = await callApi(appointment)
      expect(res1.status).toBe(BAD_REQUEST);
    });
	});



});
