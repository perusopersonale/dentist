import logger from './Logger';


export const pErr = (err: Error) => {
	if (err) {
		logger.err(err);
	}
};


export const getRandomInt = () => {
	return Math.floor(Math.random() * 1_000_000_000_000);
};

export const addDayToDate = ( date: Date, days: number) => {
	const newDate = new Date(date)
	newDate.setDate(newDate.getDate()+days);
	return newDate
}
export const addHoursToDate = ( date: Date, hours: number) => {
	const newDate = new Date(date)
	newDate.setTime(newDate.getTime() + (hours*60*60*1000));
	return newDate
}