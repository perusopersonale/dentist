import bcrypt from 'bcrypt';
import StatusCodes from 'http-status-codes';
import { Request, Response, Router } from 'express';

import validate from '@middlewares/validator';
import User, { UserRoles } from '@models/User';
import { restrictAccessTo } from '@middlewares/auth';
import { pwdSaltRounds } from '@utils/constants';
import logger from '@utils/Logger';

const router = Router();
const { BAD_REQUEST,  OK , NOT_FOUND} = StatusCodes;


/******************************************************************************
 *                      Get All Users - "GET /api/current/"
 ******************************************************************************/

router.get('/', restrictAccessTo(UserRoles.Standard), async (req: Request, res: Response) => {
	return res.json({data : {...req.user}});
});




/******************************************************************************
 *                       Update - "PUT /api/current"
 ******************************************************************************/

 router.put('/', restrictAccessTo(UserRoles.Standard), validate('updateUser'), async (req: Request, res: Response) => {
	await User.updateOne({id: req.user.id}, req.body)
	return res.status(OK).end();
});




/******************************************************************************
 *                                     Export
 ******************************************************************************/

export default router;
