import mongoose, { Schema, Document } from 'mongoose';

const START_HOUR = 9
const CLOSE_HOUR = 20
const SATURDAY = 6
const SUNDAY = 0

export interface IAppointment extends Document {
  userId: string;
  title: string;
  startDate: Date;
  endDate: Date;
  description: string;
}

const isDateInWorkTime = (date: Date) => {
  const day = date.getDay()
  const hour = date.getHours()
  // if is Sunday or Saturday or between start/close time return false
  return ! (day === SUNDAY || day === SATURDAY || hour < START_HOUR || hour > CLOSE_HOUR)
  
}

const AppointmentSchema: Schema = new Schema({
  userId: { type: String, required: true },
  startDate: { type: Date, required: true },
  endDate: { type: Date, required: true },
  title: { type: String,},
  description: { type: String },
});

AppointmentSchema.pre<IAppointment>('save', async function (next) {
  if(this.endDate <= this.startDate) {
    throw new Error('not valid interval')
  }
  if( !isDateInWorkTime(this.startDate) || !isDateInWorkTime(this.endDate)){
    throw new Error('date not in working time')
  }

  const Appointment = mongoose.connection.model('Appointment')
  const collision = await Appointment.findOne({ 
    $or: [
      {
        startDate: {
          $gte: this.startDate,
          $lt: this.endDate
        }
      },
      {
        endDate: {
          $gt : this.startDate,
          $lt: this.endDate
        }
      }
    ]
  })
  if(collision) throw new Error('collision detected')
  next()
})
// Export the model and return your IAppointment interface
export default mongoose.model<IAppointment>('Appointment', AppointmentSchema);