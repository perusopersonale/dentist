import StatusCodes from 'http-status-codes';
import { Request, Response, NextFunction } from 'express';

import  { UserRoles } from '@models/User';
import { cookieProps } from '@utils/constants';
import { JwtService } from '@utils/JwtService';
import logger from '@utils/Logger';



const jwtService = new JwtService();
const { UNAUTHORIZED } = StatusCodes;


const getUserFromToken = async (req: Request) => {
   // Get json-web-token
   const jwt = req.signedCookies[cookieProps.key];
   if (!jwt) {
       throw Error('Unauthorized');
   }
   return await jwtService.decodeJwt(jwt);
}

export function restrictAccessTo(role: UserRoles) {
  return async (req: Request, res: Response, next: NextFunction) => {
    try {
      const user = await getUserFromToken(req)
      // simple auth if admin pass or if user role is the same ad param
      if(user.role === UserRoles.Admin || role === user.role) {
        req.user = user;
        next();
      }
      else {
        throw new Error('Unhautorized')
      }
      
    } catch (err) {
      return res.status(UNAUTHORIZED).json({
        error: err.message,
      });
    } 
  }
}
