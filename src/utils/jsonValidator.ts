import Ajv from 'ajv'
import addFormats from 'ajv-formats'
import glob from 'glob'
import { readFileSync } from 'jsonfile'
import { paramMissingError } from './constants'
import logger from './Logger'

const GLOB_JSONSCHEMA_PATTERN = "**/jsonSchemas/**/*.json"

class JsonValidator {
  private validator: Ajv
  constructor(){
    this.validator = new Ajv({
      allErrors: true,
    })
    addFormats(this.validator)
  }
  /**
   *  init
   * 
   *  Load jsonschema from folder syncronously and add to validator.
   *  
   */
  public init(){
    const schemas = glob.sync(GLOB_JSONSCHEMA_PATTERN)
    schemas.forEach( s => {
      const jschema = readFileSync(s)
      logger.info(`Registering schema :${s} width id: ${jschema.$id}`)
      this.validator.addSchema(jschema, jschema.$id)
    })
  }
   /**
   *  validate
   *  
   *  @param schema id of the schema to validate against
   *  @param data object to validate
   * 
   *  Validate object against a schema otherwise throw error
   *  
   */
  validate(schema: string, data: object) {
    if(!this.validator.validate(schema, data)){
      logger.err(this.validator.errors)
      throw new Error(paramMissingError)
    }
  }
}

export default new JsonValidator()