import mongoose, { Schema, Document } from 'mongoose';

export enum UserRoles {
  Standard,
  Admin,
}

  
export interface IUser extends Document {
  email: string;
  firstName: string;
  lastName: string;
  pwdHash: string;
  role: UserRoles;
}

const UserSchema: Schema = new Schema({
  email: { type: String, required: true, unique: true },
  pwdHash: { type: String, required: true },
  firstName: { type: String, required: true },
  lastName: { type: String, required: true },
  role: { type: Number, enum: Object.values(UserRoles), default: UserRoles.Standard },
  
});

// Export the model and return your IUser interface
export default mongoose.model<IUser>('User', UserSchema);