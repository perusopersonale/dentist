import logger from '@utils/Logger';
import mongoose from 'mongoose';

const connectToDb = async () => {
  try {
    await mongoose.connect(
      String(process.env.DB_CONNECTION_STRING),
      { useNewUrlParser: true }
    )
    logger.info(`Successfully connected to db`);
  }catch(err){
    logger.err('Error connecting to database: ', err);
    return process.exit(1);
  }
};

export default function connect() {  
  connectToDb();
};