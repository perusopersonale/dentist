import { Router } from 'express';
import UserRouter from './Users';
import AppointmentRouter from './Appointments';
import ProfileRouter from './Profile';
import AuthRouter from './Auth';

// Init router and path
const router = Router();

// Add sub-routes
router.use('/users', UserRouter);
router.use('/auth', AuthRouter);
router.use('/appointments', AppointmentRouter);
router.use('/profile', ProfileRouter);

// Export the base-router
export default router;
